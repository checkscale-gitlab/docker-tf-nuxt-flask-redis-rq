'''-----------------------------------------------------------------------------
IMPORTS
-----------------------------------------------------------------------------'''
import os
# for main flask app
from flask import Flask
from flask_cors import CORS

# blueprints of the app
from .api.views import (bp as bp_api)
from . import settings
from pyapp import __name__, __version__

def create_app():

    app = Flask(__name__)
    app.register_blueprint(bp_api)#, url_prefix='/')
    app.config.from_object(settings)
    CORS(app)
    return app
