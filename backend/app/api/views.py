import redis
from flask import Blueprint, request, jsonify, current_app, g, url_for
from rq import push_connection, pop_connection, Queue
from pyapp import __name__, __version__

from .utils import do_something, predict_model

bp = Blueprint('api', __name__)

def get_redis_connection():
    redis_connection = getattr(g, '_redis_connection', None)
    if redis_connection is None:
        redis_url = current_app.config['REDIS_URL']
        redis_connection = g._redis_connection = redis.from_url(redis_url)
    return redis_connection

@bp.before_request
def push_rq_connection():
    push_connection(get_redis_connection())

@bp.teardown_request
def pop_rq_connection(exception=None):
    pop_connection()

@bp.route('/task/<task_id>', methods=['GET'])
def check_task(task_id):
    q = Queue()
    task = q.fetch_job(task_id)
    if task is None:
        response = {
            'status': 'unknown',
            'message': 'no task with id: {}'.format(task_id)
        }

    else:
        response = {
            'task_id': task.get_id(),
            'status': task.get_status(),
            'result': task.result,
        }
        if task.is_failed:
            msg = task.exc_info.strip().split('\n')[-1]
            response['message'] = msg
    return jsonify(response)

@bp.route('/model-task', methods=['GET'])
def model_task():
    uri = 'model-task'
    q = Queue()
    task = q.enqueue(predict_model)

    res_uri = 'task/{}'.format(task.get_id())
    response = {
        'status': 'success',
        'data': {
            'task_id': task.get_id(),
            'result_uri': res_uri,
            'uri': uri,
        }
    }

    return jsonify(response)

@bp.route('/demo-task', methods=['GET'])
def run_task():
    uri = 'demo-task'
    q = Queue()
    task = q.enqueue(do_something, '4u')

    res_uri = 'task/{}'.format(task.get_id())
    response = {
        'status': 'success',
        'data': {
            'task_id': task.get_id(),
            'result_uri': res_uri,
            'uri': uri,
        }
    }

    return jsonify(response)


@bp.route('/')
def index():
    return '{} server'.format(__name__)
